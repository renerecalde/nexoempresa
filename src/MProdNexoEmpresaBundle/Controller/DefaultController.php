<?php

namespace MProdNexoEmpresaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        $page_title = $this->getParameter('page_title');
        return $this->render('Default/index.html.twig', array('page_title' => $page_title));
    }
}
