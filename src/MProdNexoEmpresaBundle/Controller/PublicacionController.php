<?php

namespace MProdNexoEmpresaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use MProdNexoEmpresaBundle\Entity\Publicacion;
use MProdNexoEmpresaBundle\Entity\ImagenPublicacion;


/**
 * Publicacion controller.
 *
 * @Route("/publicacion")
 */
class PublicacionController extends Controller
{
    /**
     * Lists all Publicacion entities.
     *
     * @Route("/", name="publicacion")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('MProdNexoEmpresaBundle:Publicacion')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($publicacions, $pagerHtml) = $this->paginator($queryBuilder, $request);

        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('publicacion/index.html.twig', array(
            'publicacions' => $publicacions,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('MProdNexoEmpresaBundle\Form\PublicacionFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('PublicacionControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('PublicacionControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('PublicacionControllerFilter')) {
                $filterData = $session->get('PublicacionControllerFilter');

                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }

                $filterForm = $this->createForm('MProdNexoEmpresaBundle\Form\PublicacionFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }

        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('publicacion', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }



    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }



    /**
     * Displays a form to create a new Publicacion entity.
     *
     * @Route("/new", name="publicacion_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $publicacion = new Publicacion();
        $form   = $this->createForm('MProdNexoEmpresaBundle\Form\PublicacionType', $publicacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($publicacion);
            $em->flush();

            $editLink = $this->generateUrl('publicacion_edit', array('id' => $publicacion->getId()));
            $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New publicacion was created successfully.</a>" );

            $nextAction=  $request->get('submit') == 'save' ? 'publicacion' : 'publicacion_new';
            return $this->redirectToRoute($nextAction);
        }
        return $this->render('publicacion/new.html.twig', array(
            'publicacion' => $publicacion,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Publicacion entity.
     *
     * @Route("/newProduct", name="publicacion_new_producto")
     * @Method({"GET", "POST"})
     */
    public function newProductPublicacionAction(Request $request)
    {
      return $this->newPublicacion($request, 'Producto');
    }

    /**
     * Displays a form to create a new Publicacion entity.
     *
     * @Route("/newServicio", name="publicacion_new_servicio")
     * @Method({"GET", "POST"})
     */
    public function newServicioPublicacionAction(Request $request)
    {
      return $this->newPublicacion($request, 'Servicio');
    }

    /**
     * Displays a form to create a new Publicacion entity.
     *
     * @Route("/newProductAndService", name="publicacion_new_producto_servicio")
     * @Method({"GET", "POST"})
     */
    public function newProductoServicioPublicacionAction(Request $request)
    {
      return $this->newPublicacion($request, 'Producto y servicio');
    }

    private function newPublicacion($request, $tipo){
      $user = $this->getUser();
      $publicacion = new Publicacion();
      $publicacion->setTipo($tipo);
      $publicacion->setEmpresa($user);
      $publicacion->setCreateAt(new \DateTime());
      //$publicacion->setUpdateAt(false);


      $form   = $this->createForm('MProdNexoEmpresaBundle\Form\PublicacionProductoType', $publicacion);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
           $em = $this->getDoctrine()->getManager();
           $em->persist($publicacion);
           $em->flush();

           $editLink = $this->generateUrl('publicacion_edit', array('id' => $publicacion->getId()));
           $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New publicacion was created successfully.</a>" );

           $nextAction=  $request->get('submit') == 'save' ? 'publicacion' : 'publicacion_new';
           return $this->redirectToRoute($nextAction);
      }
       //seguir con el newProducto....
      return $this->render('publicacion/newProducto.html.twig', array(
           'publicacion' => $publicacion,
           'form'   => $form->createView(),
      ));
    }

    /**
     * Displays a form to create a new Publicacion entity.
     *
     * @Route("/newImagen", name="publicacion_new_imagen")
     * @Method({"GET", "POST"})
     */
    public function newImagen(Request $request){
      $imagen = new ImagenPublicacion();
      $publicacion = new Publicacion();
      $imagen->setPublicacion($publicacion);

      $form   = $this->createForm('MProdNexoEmpresaBundle\Form\ImagenPublicacionType', $imagen);


      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

      }
      return $this->render('publicacion/newImagen.html.twig', array(
          'form'   => $form->createView()
      ));
    }

    /**
     * Finds and displays a Publicacion entity.
     *
     * @Route("/{id}", name="publicacion_show")
     * @Method("GET")
     */
    public function showAction(Publicacion $publicacion)
    {
        $deleteForm = $this->createDeleteForm($publicacion);
        return $this->render('publicacion/show.html.twig', array(
            'publicacion' => $publicacion,
            'delete_form' => $deleteForm->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing Publicacion entity.
     *
     * @Route("/{id}/edit", name="publicacion_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Publicacion $publicacion)
    {
        $deleteForm = $this->createDeleteForm($publicacion);
        $editForm = $this->createForm('MProdNexoEmpresaBundle\Form\PublicacionType', $publicacion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($publicacion);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('publicacion_edit', array('id' => $publicacion->getId()));
        }
        return $this->render('publicacion/edit.html.twig', array(
            'publicacion' => $publicacion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }



    /**
     * Deletes a Publicacion entity.
     *
     * @Route("/{id}", name="publicacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Publicacion $publicacion)
    {

        $form = $this->createDeleteForm($publicacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($publicacion);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Publicacion was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Publicacion');
        }

        return $this->redirectToRoute('publicacion');
    }

    /**
     * Creates a form to delete a Publicacion entity.
     *
     * @param Publicacion $publicacion The Publicacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Publicacion $publicacion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('publicacion_delete', array('id' => $publicacion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Delete Publicacion by id
     *
     * @Route("/delete/{id}", name="publicacion_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(Publicacion $publicacion){
        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($publicacion);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Publicacion was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Publicacion');
        }

        return $this->redirect($this->generateUrl('publicacion'));

    }


    /**
    * Bulk Action
    * @Route("/bulk-action/", name="publicacion_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('MProdNexoEmpresaBundle:Publicacion');

                foreach ($ids as $id) {
                    $publicacion = $repository->find($id);
                    $em->remove($publicacion);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'publicacions was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the publicacions ');
            }
        }

        return $this->redirect($this->generateUrl('publicacion'));
    }


}
