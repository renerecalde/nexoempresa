<?php
// src/MProdNexoEmpresaBundle/Controller/RegistrationController.php

namespace MProdNexoEmpresaBundle\Controller;

use MProdNexoEmpresaBundle\Entity\Empresa;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// use SoapClient;

/**
 * Registration controller.
 *
 * @Route("register")
 */
class RegistrationController extends BaseController
{
    // private $username;
    // private $password;

    public function __construct()
    {

    }

    // /*Generates de WSSecurity header*/
    // private function wssecurity_header(){

    //     $timestamp=gmdate('Y-m-d\TH:i:s\Z');//The timestamp. The computer must be on time or the server you are connecting may reject the password digest for security.
    //     $nonce=mt_rand(); //A random word. The use of rand() may repeat the word if the server is very loaded.
    //     $passdigest=base64_encode(pack('H*',sha1(pack('H*',$nonce).pack('a*',$timestamp).pack('a*',$this->password))));//This is the right way to create the password digest. Using the password directly may work also, but it's not secure to transmit it without encryption. And anyway, at least with axis+wss4j, the nonce and timestamp are mandatory anyway.

    //     $auth='
    //         <wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    //         <wsse:UsernameToken>
    //             <wsse:Username>'.$this->username.'</wsse:Username>
    //             <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigest.'</wsse:Password>
    //             <wsse:Nonce>'.base64_encode(pack('H*',$nonce)).'</wsse:Nonce>
    //             <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'.$timestamp.'</wsu:Created>
    //            </wsse:UsernameToken>
    //         </wsse:Security>
    //     ';

    //     $authvalues=new SoapVar($auth,XSD_ANYXML); //XSD_ANYXML (or 147) is the code to add xml directly into a SoapVar. Using other codes such as SOAP_ENC, it's really difficult to set the correct namespace for the variables, so the axis server rejects the xml.
    //     $header=new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security",$authvalues,true);

    //     return $header;
    // }

    // /*It's necessary to call it if you want to set a different user and password*/
    // public function __setUsernameToken($username,$password){
    //     $this->username=$username;
    //     $this->password=$password;
    // }


    // /*Overwrites the original method adding the security header. As you can see, if you want to add more headers, the method needs to be modifyed*/
    // public function __soapCall($function_name,$arguments,$options=null,$input_headers=null,&$output_headers=null){
    //     $result = parent::__soapCall($function_name,$arguments,$options,$this->wssecurity_header());
    //     return $result;
    // }

    /**
     * @Route("/", name="registro")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function registerAction(Request $request)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                
                $user->setCreatedAt(new \DateTime('now'));
                $user->setEstado('OK');
                $empresa = new Empresa();
                $em = $this->getDoctrine()->getManager();
                $em->persist($empresa);
                $cuit = $form->get('cuit')->getData();

                // $w_client_datosContrib = new SoapClient(null, array(
                //     'location' => 'https://aswe.santafe.gov.ar/proxy.php/API/w-datoscontrib',
                //     'uri' => "urn:wsServices",
                //     'trace' => 1,
                //     'encoding' => 'utf-8',
                //     'exceptions' => 0)
                // );
                // $w_client_datosContrib->__setUsernameToken("#usDaCon2#", "%A21d62%");
                // $parametros = array($cuit, 'T');
                // $result = $w_client_datosContrib->__soapCall("getContribuyente", $parametros, array());
                $empresa->setCuit($cuit);
                $empresa->setLogo('');
                // if($result['codigo_respuesta']==200){
                //     $empresa->setRazonSocial($result);
                //     $empresa->setIsSantafecina('SI');
                // }
                // else{
                    $empresa->setRazonSocial('');
                    $empresa->setIsSantafecina('NO');
                // }
                $empresa->setIngresosBrutos('');
                $empresa->setActividadInscripta('');
                $empresa->setLogoName('');
                $empresa->setLogoSize(1);
                $empresa->setLogoUpdatedAt(new \DateTime('now'));
                $em->flush();                
                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Registration/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
