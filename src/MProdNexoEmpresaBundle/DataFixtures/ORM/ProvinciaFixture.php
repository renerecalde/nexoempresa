<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 namespace MProdNexoEmpresaBundle\DataFixtures\ORM;

 use MProdNexoEmpresaBundle\Entity\Provincia;
 use Doctrine\Common\DataFixtures\FixtureInterface;
 use Doctrine\Common\Persistence\ObjectManager;


class ProvinciaFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $provincias = array(
          array('id' => 1, 'nombre' => 'CABA'),
          array('id' => 2, 'nombre' => 'BUENOS AIRES'),
          array('id' => 3, 'nombre' => 'CATAMARCA'),
          array('id' => 4, 'nombre' => 'CHACO'),
          array('id' => 5, 'nombre' => 'CHUBUT'),
          array('id' => 6, 'nombre' => 'CORDOBA'),
          array('id' => 7, 'nombre' => 'CORRIENTES'),
          array('id' => 8, 'nombre' => 'ENTRE RIOS'),
          array('id' => 9, 'nombre' => 'FORMOSA'),
          array('id' => 10, 'nombre' => 'JUJUY'),
          array('id' => 11, 'nombre' => 'LA PAMPA'),
          array('id' => 12, 'nombre' => 'LA RIOJA'),
          array('id' => 13, 'nombre' => 'MENDOZA'),
          array('id' => 14, 'nombre' => 'MISIONES'),
          array('id' => 15, 'nombre' => 'NEUQUEN'),
          array('id' => 16, 'nombre' => 'RIO NEGRO'),
          array('id' => 17, 'nombre' => 'SALTA'),
          array('id' => 18, 'nombre' => 'SAN JUAN'),
          array('id' => 19, 'nombre' => 'SAN LUIS'),
          array('id' => 20, 'nombre' => 'SANTA CRUZ'),
          array('id' => 21, 'nombre' => 'SANTA FE'),
          array('id' => 22, 'nombre' => 'SANTIAGO DEL ESTERO'),
          array('id' => 23, 'nombre' => 'TIERRA DEL FUEGO, ANTARTIDA E ISLAS DEL A.S.'),
          array('id' => 24, 'nombre' => 'TUCUMAN')
            // ...
        );
        foreach ($provincias as $provincia) {
            $entidad = new Provincia();
            $entidad->setId($provincia['id']);
            $entidad->setNombre($provincia['nombre']);

            $manager->persist($entidad);
        }
        $manager->flush();

    }

}
