<?php
// src/DataFixtures/AppFixtures.php
namespace MProdNexoEmpresaBundle\DataFixtures\ORM;

use MProdNexoEmpresaBundle\Entity\Usuario;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class UsuarioFixture implements FixtureInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {

            $usuario = new Usuario();
            $usuario->setEstado('Estado');
            $usuario->setRoles(array('ROLE_SUPER_ADMIN'));
            $usuario->setCreatedAt(new \DateTime());
            $usuario->setEmail("rrecalde@santafe.gov.ar");
            $usuario->setUserName('rrene');
            $password = $this->encoder->encodePassword($usuario, 'pass_1234');
            $usuario->setPassword($password);
            $usuario->setEnabled(true);
            $manager->persist($usuario);

            $usuario = new Usuario();
            $usuario->setEstado('Estado');
            $usuario->setRoles(array('ROLE_SUPER_ADMIN'));
            $usuario->setCreatedAt(new \DateTime());
            $usuario->setEmail("ageespinosa@santafe.gov.ar");
            $usuario->setUserName('ageespinosa');
            $usuario->setPassword("gustavo");
            $password = $this->encoder->encodePassword($usuario, 'pass_1234');
            $usuario->setPassword($password);
            $usuario->setEnabled(true);
            $manager->persist($usuario);

            $usuario = new Usuario();
            $usuario->setEstado('Estado');
            $usuario->setRoles(array('ROLE_SUPER_ADMIN'));
            $usuario->setCreatedAt(new \DateTime());
            $usuario->setEmail("sonial@santafe.gov.ar");
            $usuario->setUserName('sonial');
            $usuario->setPassword("sonial");
            $password = $this->encoder->encodePassword($usuario, 'pass_1234');
            $usuario->setPassword($password);
            $usuario->setEnabled(true);
            $manager->persist($usuario);



        $manager->flush();
    }
}
