<?php

namespace MProdNexoEmpresaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use MProdNexoEmpresaBundle\Entity\Empresa;
use MProdNexoEmpresaBundle\Entity\Pedido;


/**
 * Cotizacion
 *
 * @ORM\Table(name="cotizacion")
 * @ORM\Entity(repositoryClass="MProdNexoEmpresaBundle\Repository\CotizacionRepository")
 */
class Cotizacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     * @Assert\Type(
     *     type="string",
     *     message="No es un tipo válido."
     * )
     *
     */
    private $descripcion;

    // /**
    //  * @var string
    //  *
    //  * @ORM\Column(name="archivo", type="string", length=255)
    //  */
    // private $archivo;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\File(
     *     maxSize="4M",
     *     mimeTypes={"application/msword", "application/pdf", "image/jpeg", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.oasis.opendocument.text" }
     * )
     * @Vich\UploadableField(mapping="empresa_logo", fileNameProperty="logoName")
     *
     * @var File
     */
    private $archivoFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $logoName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $archivoSize;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $archivoUpdatedAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */




    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime")
     * @Assert\DateTime
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime")
     * @Assert\DateTime
     */
    private $updateAt;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity="Pedido")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     */
    private $pedido;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Cotizacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set archivo
     *
     * @param string $archivo
     *
     * @return Cotizacion
     */
    public function setArchivo($archivo)
    {
        $this->archivo = $archivo;

        return $this;
    }

    /**
     * Get archivo
     *
     * @return string
     */
    public function getArchivo()
    {
        return $this->archivo;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Cotizacion
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Cotizacion
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }



    /**
     * Set empresa
     *
     * @param \MProdNexoEmpresaBundle\Entity\Empresa $empresa
     *
     * @return Cotizacion
     */
    public function setEmpresa(\MProdNexoEmpresaBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \MProdNexoEmpresaBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set pedido
     *
     * @param \MProdNexoEmpresaBundle\Entity\Pedido $pedido
     *
     * @return Cotizacion
     */
    public function setPedido(\MProdNexoEmpresaBundle\Entity\Pedido $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MProdNexoEmpresaBundle\Entity\Pedido
     */
    public function getPedido()
    {
        return $this->pedido;
    }



    /**
     * Set logoName
     *
     * @param string $logoName
     *
     * @return Cotizacion
     */
    public function setLogoName($logoName)
    {
        $this->logoName = $logoName;

        return $this;
    }

    /**
     * Get logoName
     *
     * @return string
     */
    public function getLogoName()
    {
        return $this->logoName;
    }

    /**
     * Set archivoSize
     *
     * @param integer $archivoSize
     *
     * @return Cotizacion
     */
    public function setArchivoSize($archivoSize)
    {
        $this->archivoSize = $archivoSize;

        return $this;
    }

    /**
     * Get archivoSize
     *
     * @return integer
     */
    public function getArchivoSize()
    {
        return $this->archivoSize;
    }

    /**
     * Set archivoUpdatedAt
     *
     * @param \DateTime $archivoUpdatedAt
     *
     * @return Cotizacion
     */
    public function setArchivoUpdatedAt($archivoUpdatedAt)
    {
        $this->archivoUpdatedAt = $archivoUpdatedAt;

        return $this;
    }

    /**
     * Get archivoUpdatedAt
     *
     * @return \DateTime
     */
    public function getArchivoUpdatedAt()
    {
        return $this->archivoUpdatedAt;
    }

    public function __toString()
    {
      return $this->getId()." ". $this->getDescripcion();
    }
}
