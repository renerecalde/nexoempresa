<?php

namespace MProdNexoEmpresaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use MProdNexoEmpresaBundle\Entity\Empresa;

/**
 * DatoContacto
 *
 * @ORM\Table(name="dato_contacto")
 * @ORM\Entity(repositoryClass="MProdNexoEmpresaBundle\Repository\DatoContactoRepository")
 */
class DatoContacto
{
  const SEXO = ['m', 'f'];




    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     * @Assert\Type("string")
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono_celular", type="string", length=255)
     * @Assert\Type("string")
     */
    private $telefonoCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="responsable", type="string", length=255)
     * @Assert\Type("string")
     */
    private $responsable;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo_responsable", type="string", length=255)
     * @Assert\Choice(choices=DatoContacto::SEXO, message="Elija un sexo válido.")
     */
    private $sexoResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="calle", type="string", length=255)
     * @Assert\Type("string")
     */
    private $calle;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     * @Assert\Type("string")
     */
    private $numero;


    /**
     * @var float
     *
     * @ORM\Column(name="latitud", type="float")
     * @Assert\Type(
     *     type="float",
     *     message="El valor ingresado es inválido."
     * )
     */
    private $latitud;

    /**
     * @var float
     *
     * @ORM\Column(name="longitud", type="float")
     * @Assert\Type(
     *     type="float",
     *     message="El valor ingresado es inválido."
     * )
     */
    private $longitud;

    /**
     * @var string
     *
     * @ORM\Column(name="sitioWeb", type="string", length=255)
     * @Assert\Url
     */
    private $sitioWeb;


    /**
      * @ORM\ManyToOne(targetEntity="Empresa", cascade={"remove"})
      * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id", onDelete="CASCADE")
      */
    private $empresa;

    /**
      * @ORM\ManyToOne(targetEntity="Localidad")
      * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
      */
    private $localidad;

    /**
      * @ORM\ManyToOne(targetEntity="Provincia")
      * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
      */
    private $provincia;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return DatoContacto
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set telefonoCelular
     *
     * @param string $telefonoCelular
     *
     * @return DatoContacto
     */
    public function setTelefonoCelular($telefonoCelular)
    {
        $this->telefonoCelular = $telefonoCelular;

        return $this;
    }

    /**
     * Get telefonoCelular
     *
     * @return string
     */
    public function getTelefonoCelular()
    {
        return $this->telefonoCelular;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     *
     * @return DatoContacto
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set sexoResponsable
     *
     * @param string $sexoResponsable
     *
     * @return DatoContacto
     */
    public function setSexoResponsable($sexoResponsable)
    {
        $this->sexoResponsable = $sexoResponsable;

        return $this;
    }

    /**
     * Get sexoResponsable
     *
     * @return string
     */
    public function getSexoResponsable()
    {
        return $this->sexoResponsable;
    }

    /**
     * Set calle
     *
     * @param string $calle
     *
     * @return DatoContacto
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return DatoContacto
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set localidad
     *
     * @param string $localidad
     *
     * @return DatoContacto
     */


    /**
     * Set latitud
     *
     * @param float $latitud
     *
     * @return DatoContacto
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return float
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param float $longitud
     *
     * @return DatoContacto
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return float
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set sitioWeb
     *
     * @param string $sitioWeb
     *
     * @return DatoContacto
     */
    public function setSitioWeb($sitioWeb)
    {
        $this->sitioWeb = $sitioWeb;

        return $this;
    }

    /**
     * Get sitioWeb
     *
     * @return string
     */
    public function getSitioWeb()
    {
        return $this->sitioWeb;
    }

    /**
     * Set empresa
     *
     * @param \MProdNexoEmpresaBundle\Entity\Empresa $empresa
     *
     * @return DatoContacto
     */
    public function setEmpresa(\MProdNexoEmpresaBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \MProdNexoEmpresaBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }
    public function __toString()
    {
      return $this->getId(). " ". $this->getResponsable();
    }

    /**
     * Set localidad
     *
     * @param \MProdNexoEmpresaBundle\Entity\Localidad $localidad
     *
     * @return DatoContacto
     */
    public function setLocalidad(\MProdNexoEmpresaBundle\Entity\Localidad $localidad = null)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return \MProdNexoEmpresaBundle\Entity\Localidad
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set provincia
     *
     * @param \MProdNexoEmpresaBundle\Entity\Provincia $provincia
     *
     * @return DatoContacto
     */
    public function setProvincia(\MProdNexoEmpresaBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return \MProdNexoEmpresaBundle\Entity\Provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }
}
