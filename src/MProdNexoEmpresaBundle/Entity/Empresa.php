<?php

namespace MProdNexoEmpresaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * Empresa
 *
 * @ORM\Table(name="empresa")
 * @ORM\Entity(repositoryClass="MProdNexoEmpresaBundle\Repository\EmpresaRepository")
 * @Vich\Uploadable
 */
class Empresa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cuit", type="string", length=11, unique=true)
     * @Assert\Type("digit")
     */
    private $cuit;

  //  /**
  //   * @var string
  //   *
  //   * @ORM\Column(name="logo", type="string", length=255)
  //   */
  //  private $logo;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\File(
     *     maxSize="4M",
     *     mimeTypes={"application/msword", "application/pdf", "image/jpeg", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.oasis.opendocument.text" }
     * )
     * @Vich\UploadableField(mapping="empresa_logo", fileNameProperty="logoName", size="logoSize")
     *
     * @var File
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $logoName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $logoSize;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $logoUpdatedAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */







    /**
     * @var string
     *
     * @ORM\Column(name="razon_social", type="string", length=255)
     * @Assert\Type("string")
     */
    private $razonSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="ingresos_brutos", type="string", length=255, unique=true)
     * @Assert\Type("string")
     */
    private $ingresosBrutos;

    /**
     * @var string
     *
     * @ORM\Column(name="actividad_inscripta", type="string", length=255)
     * @Assert\Type("string")
     */
    private $actividadInscripta;

    /**
     * @var string
     *
     * @ORM\Column(name="is_santafecina", type="string", length=255)
     * @Assert\Type("boolean")
     */
    private $isSantafecina;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     *
     * @return Empresa
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;

        return $this;
    }

    /**
     * Get cuit
     *
     * @return string
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Empresa
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     *
     * @return Empresa
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set ingresosBrutos
     *
     * @param string $ingresosBrutos
     *
     * @return Empresa
     */
    public function setIngresosBrutos($ingresosBrutos)
    {
        $this->ingresosBrutos = $ingresosBrutos;

        return $this;
    }

    /**
     * Get ingresosBrutos
     *
     * @return string
     */
    public function getIngresosBrutos()
    {
        return $this->ingresosBrutos;
    }

    /**
     * Set actividadInscripta
     *
     * @param string $actividadInscripta
     *
     * @return Empresa
     */
    public function setActividadInscripta($actividadInscripta)
    {
        $this->actividadInscripta = $actividadInscripta;

        return $this;
    }

    /**
     * Get actividadInscripta
     *
     * @return string
     */
    public function getActividadInscripta()
    {
        return $this->actividadInscripta;
    }

    /**
     * Set logoName
     *
     * @param string $logoName
     *
     * @return Empresa
     */
    public function setLogoName($logoName)
    {
        $this->logoName = $logoName;

        return $this;
    }

    /**
     * Get logoName
     *
     * @return string
     */
    public function getLogoName()
    {
        return $this->logoName;
    }

    /**
     * Set logoSize
     *
     * @param integer $logoSize
     *
     * @return Empresa
     */
    public function setLogoSize($logoSize)
    {
        $this->logoSize = $logoSize;

        return $this;
    }

    /**
     * Get logoSize
     *
     * @return integer
     */
    public function getLogoSize()
    {
        return $this->logoSize;
    }

    /**
     * Set logoUpdatedAt
     *
     * @param \DateTime $logoUpdatedAt
     *
     * @return Empresa
     */
    public function setLogoUpdatedAt($logoUpdatedAt)
    {
        $this->logoUpdatedAt = $logoUpdatedAt;

        return $this;
    }

    /**
     * Get logoUpdatedAt
     *
     * @return \DateTime
     */
    public function getLogoUpdatedAt()
    {
        return $this->logoUpdatedAt;
    }

    public function __toString()
    {
      return $this->getId(). " ". $this->getCuit();
    }

    /**
     * Set isSantafecina
     *
     * @param string $isSantafecina
     *
     * @return Empresa
     */
    public function setIsSantafecina($isSantafecina)
    {
        $this->isSantafecina = $isSantafecina;

        return $this;
    }

    /**
     * Get isSantafecina
     *
     * @return string
     */
    public function getIsSantafecina()
    {
        return $this->isSantafecina;
    }
}
