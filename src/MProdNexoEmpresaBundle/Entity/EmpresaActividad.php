<?php

namespace MProdNexoEmpresaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * EmpresaTag
 *
 * @ORM\Table(name="empresa_actividad")
 * @ORM\Entity(repositoryClass="MProdNexoEmpresaBundle\Repository\EmpresaActividadRepository")
 */
class EmpresaActividad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     */
    private $empresa;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Actividad")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id")
     */
    private $actividad;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }





    /**
     * Set empresa
     *
     * @param \MProdNexoEmpresaBundle\Entity\Empresa $empresa
     *
     * @return EmpresaActividad
     */
    public function setEmpresa(\MProdNexoEmpresaBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \MProdNexoEmpresaBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set actividad
     *
     * @param \MProdNexoEmpresaBundle\Entity\Actividad $actividad
     *
     * @return EmpresaActividad
     */
    public function setActividad(\MProdNexoEmpresaBundle\Entity\Actividad $actividad = null)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return \MProdNexoEmpresaBundle\Entity\Actividad
     */
    public function getActividad()
    {
        return $this->actividad;
    }
}
