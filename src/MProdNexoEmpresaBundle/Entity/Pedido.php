<?php

namespace MProdNexoEmpresaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pedido
 *
 * @ORM\Table(name="pedido")
 * @ORM\Entity(repositoryClass="MProdNexoEmpresaBundle\Repository\PedidoRepository")
 */
class Pedido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="necesito", type="string", length=255)
     * @Assert\Type("string")
     */
    private $necesito;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     * @Assert\Type("string")
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float")
     * @Assert\Type("float")
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="lugarEntrega", type="string", length=255)
     * @Assert\Type("string")
     */
    private $lugarEntrega;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vigencia_desde", type="datetime")
     * @Assert\DateTime
     */
    private $vigenciaDesde;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vigencia_hasta", type="datetime")
     * @Assert\DateTime
     */
    private $vigenciaHasta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime")
     * @Assert\DateTime
     */
    private $updateAt;

    /**
     * @ORM\ManyToMany(targetEntity="Actividad", inversedBy="pedidos")
     */
    private $actividades;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set necesito
     *
     * @param string $necesito
     *
     * @return Pedido
     */
    public function setNecesito($necesito)
    {
        $this->necesito = $necesito;

        return $this;
    }

    /**
     * Get necesito
     *
     * @return string
     */
    public function getNecesito()
    {
        return $this->necesito;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Pedido
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     *
     * @return Pedido
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set lugarEntrega
     *
     * @param string $lugarEntrega
     *
     * @return Pedido
     */
    public function setLugarEntrega($lugarEntrega)
    {
        $this->lugarEntrega = $lugarEntrega;

        return $this;
    }

    /**
     * Get lugarEntrega
     *
     * @return string
     */
    public function getLugarEntrega()
    {
        return $this->lugarEntrega;
    }

    /**
     * Set vigenciaDesde
     *
     * @param \DateTime $vigenciaDesde
     *
     * @return Pedido
     */
    public function setVigenciaDesde($vigenciaDesde)
    {        
        $this->vigenciaDesde = $vigenciaDesde;

        return $this;
    }

    /**
     * Get vigenciaDesde
     *
     * @return \DateTime
     */
    public function getVigenciaDesde()
    {
        return $this->vigenciaDesde;
    }

    /**
     * Set vigenciaHasta
     *
     * @param \DateTime $vigenciaHasta
     *
     * @return Pedido
     */
    public function setVigenciaHasta($vigenciaHasta)
    {
        $this->vigenciaHasta = $vigenciaHasta;

        return $this;
    }

    /**
     * Get vigenciaHasta
     *
     * @return \DateTime
     */
    public function getVigenciaHasta()
    {
        return $this->vigenciaHasta;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pedido
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Pedido
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }
    public function __toString()
    {
      return $this->getId(). " ". $this->getNecesito();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actividades = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add actividad
     *
     * @param \MProdNexoEmpresaBundle\Entity\Actividad $actividad
     *
     * @return Pedido
     */
    public function addActividad(\MProdNexoEmpresaBundle\Entity\Actividad $actividad)
    {
        $this->actividades[] = $actividad;

        return $this;
    }

    /**
     * Remove actividad
     *
     * @param \MProdNexoEmpresaBundle\Entity\Actividad $actividad
     */
    public function removeActividad(\MProdNexoEmpresaBundle\Entity\Actividad $actividad)
    {
        $this->actividades->removeElement($actividad);
    }

    /**
     * Get actividades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActividades()
    {
        return $this->actividades;
    }
}
