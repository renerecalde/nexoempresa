<?php

namespace MProdNexoEmpresaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Publicacion
 *
 * @ORM\Table(name="publicacion")
 * @ORM\Entity(repositoryClass="MProdNexoEmpresaBundle\Repository\PublicacionRepository")
* @Vich\Uploadable
 */
class Publicacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     * @Assert\Choice({"Producto", "Servicio", "Producto y servicio"})
     */
    private $tipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable = true)
     */
    private $updateAt;


    /** Campos de imagen Foto2 **/

     /**
      * NOTE: This is not a mapped field of entity metadata, just a simple property.
      * @Assert\File(
      *     maxSize = "1024k",
      *     mimeTypes = {"image/png", "image/jpeg", "image/jpg"},
      *     mimeTypesMessage = "Please upload a valid PDF or valid IMAGE"
      * )
      * @Vich\UploadableField(mapping="imagen_publicacion_image", fileNameProperty="imageName2", size="imageSize2")
      *
      * @var File
      */
     private $imageFile2;

     /**
      * @ORM\Column(type="string", length=255)
      *
      * @var string
      */
     private $imageName2;

     /**
      * @ORM\Column(type="integer", nullable = true)
      *
      * @var integer
      */
     private $imageSize2;

     /**
      * @ORM\Column(type="datetime", nullable = true)
      *
      * @var \DateTime
      */
     private $updatedAt2;


     /** Campos de imagen **/


  /**
    * @ORM\ManyToOne(targetEntity="Empresa")
    * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
    */
    private $empresa;


   /** Campos de imagen Foto1 **/

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/png", "image/jpeg", "image/jpg"},
     *     mimeTypesMessage = "Please upload a valid PDF or valid IMAGE"
     * )
     * @Vich\UploadableField(mapping="imagen_publicacion_image", fileNameProperty="imageName1", size="imageSize1")
     *
     * @var File
     */
    private $imageFile1;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName1;

    /**
     * @ORM\Column(type="integer", nullable = true)
     *
     * @var integer
     */
    private $imageSize1;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     *
     * @var \DateTime
     */
    private $updatedAt1;


    /** Campos de imagen **/

     /**
      * NOTE: This is not a mapped field of entity metadata, just a simple property.
      * @Assert\File(
      *     maxSize = "1024k",
      *     mimeTypes = {"image/png", "image/jpeg", "image/jpg"},
      *     mimeTypesMessage = "Please upload a valid PDF or valid IMAGE"
      * )
      * @Vich\UploadableField(mapping="imagen_publicacion_image", fileNameProperty="imageName", size="imageSize")
      *
      * @var File
      */
     private $imageFile;

     /**
      * @ORM\Column(type="string", length=255)
      *
      * @var string
      */
     private $imageName;

     /**
      * @ORM\Column(type="integer", nullable = true)
      *
      * @var integer
      */
     private $imageSize;

     /**
      * @ORM\Column(type="datetime", nullable = true)
      *
      * @var \DateTime
      */
     private $updatedAt;

     /** fin de campos de la imágen **/









    /**
     *
     *************************** Getters and Setters ***************************
     **/


     /** Campos de la Imagen(Foto) **/

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;

    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
    /** end campos de imagen **/

    /** Campos de la Imagen2(Foto2) **/

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile1
     */
    public function setImageFile2(?File $imageFile2 = null): void
    {
        $this->imageFile2 = $imageFile2;

        if (null !== $imageFile2) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt2 = new \DateTimeImmutable();
        }
    }

    public function getImageFile2(): ?File
    {
        return $this->imageFile2;
    }

    public function setImageName2(?string $imageName2): void
    {
        $this->imageName2 = $imageName2;

    }

    public function getImageName2(): ?string
    {
        return $this->imageName2;
    }

    public function setImageSize2(?int $imageSize2): void
    {
        $this->imageSize2 = $imageSize2;
    }

    public function getImageSize2(): ?int
    {
        return $this->imageSize2;
    }
    /** end campos de imagen **/




     /** Campos de la Imagen1(Foto1) **/

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setImageFile1(?File $imageFile1 = null): void
    {
        $this->imageFile1 = $imageFile1;

        if (null !== $imageFile1) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt1 = new \DateTimeImmutable();
        }
    }

    public function getImageFile1(): ?File
    {
        return $this->imageFile1;
    }

    public function setImageName1(?string $imageName1): void
    {
        $this->imageName1 = $imageName1;

    }

    public function getImageName1(): ?string
    {
        return $this->imageName1;
    }

    public function setImageSize1(?int $imageSize1): void
    {
        $this->imageSize1 = $imageSize1;
    }

    public function getImageSize1(): ?int
    {
        return $this->imageSize1;
    }
    /** end campos de imagen **/



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Publicacion
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Publicacion
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return Publicacion
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set empresa
     *
     * @param \MProdNexoEmpresaBundle\Entity\Empresa $empresa
     *
     * @return Publicacion
     */
    public function setEmpresa(\MProdNexoEmpresaBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \MProdNexoEmpresaBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    public function __toString()
    {
      return $this->getId()." ". $this->getTipo();
    }
}
