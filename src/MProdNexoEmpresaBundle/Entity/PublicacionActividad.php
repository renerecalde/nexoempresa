<?php

namespace MProdNexoEmpresaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PublicacionActividad
 *
 * @ORM\Table(name="publicacion_actividad")
 * @ORM\Entity(repositoryClass="MProdNexoEmpresaBundle\Repository\PublicacionActividadRepository")
 */
class PublicacionActividad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Publicacion")
     * @ORM\JoinColumn(name="publicacion_id", referencedColumnName="id")
     */
    private $publicacion;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Actividad")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id")
     */
    private $actividad;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }




    public function __toString()
    {
      return $this->getId(). " ". $this->getPublicacion() ." ". $this->getActividad();
    }

    /**
     * Set publicacion
     *
     * @param \MProdNexoEmpresaBundle\Entity\Publicacion $publicacion
     *
     * @return PublicacionActividad
     */
    public function setPublicacion(\MProdNexoEmpresaBundle\Entity\Publicacion $publicacion = null)
    {
        $this->publicacion = $publicacion;

        return $this;
    }

    /**
     * Get publicacion
     *
     * @return \MProdNexoEmpresaBundle\Entity\Publicacion
     */
    public function getPublicacion()
    {
        return $this->publicacion;
    }



    /**
     * Set actividad
     *
     * @param \MProdNexoEmpresaBundle\Entity\Actividad $actividad
     *
     * @return PublicacionActividad
     */
    public function setActividad(\MProdNexoEmpresaBundle\Entity\Actividad $actividad = null)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return \MProdNexoEmpresaBundle\Entity\Actividad
     */
    public function getActividad()
    {
        return $this->actividad;
    }
}
