<?php

namespace MProdNexoEmpresaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class PedidoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$builder->add('necesito')->add('descripcion')->add('cantidad')->add('lugarEntrega')->add('vigenciaDesde')->add('vigenciaHasta')->add('createdAt')->add('updateAt')->add('actividades');
        $builder->add('necesito')
        ->add('descripcion')
        ->add('cantidad')
        ->add('lugarEntrega')
        ->add('vigenciaDesde', DateTimeType::class, [
            'placeholder' => [
                'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
            ]
        ])->add('vigenciaHasta', DateTimeType::class, [
            'placeholder' => [
                'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
            ]
        ])->add('actividades');
    
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MProdNexoEmpresaBundle\Entity\Pedido'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mprodnexoempresabundle_pedido';
    }


}
