<?php

namespace MProdNexoEmpresaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class PublicacionFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
            ->add('tipo', Filters\TextFilterType::class)
            ->add('createAt', Filters\DateTimeFilterType::class)
            ->add('updateAt', Filters\DateTimeFilterType::class)

            ->add('empresa', Filters\EntityFilterType::class, array(
                    'class' => 'MProdNexoEmpresaBundle\Entity\Empresa',
                    'choice_label' => 'cuit',
            ))

        ;
        $builder->setMethod("GET");


    }

    public function getBlockPrefix()
    {
        return null;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
