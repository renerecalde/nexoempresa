<?php

namespace MProdNexoEmpresaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use MProdNexoEmpresaBundle\Form\ImagenPublicacionType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PublicacionProductoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
        ->add('imageFile', VichImageType::class, [
          'required' => false,
          'allow_delete' => true,
        ])
        ->add('imageFile1', VichImageType::class, [
          'required' => false,
          'allow_delete' => true,
        ])
        ->add('imageFile2', VichImageType::class, [
          'required' => false,
          'allow_delete' => true,
        ]);




    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MProdNexoEmpresaBundle\Entity\Publicacion'
        ));
    }
}
