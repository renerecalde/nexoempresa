<?php

namespace MProdNexoEmpresaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PublicacionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo')
            ->add('createAt')
            ->add('updateAt')
            ->add('empresa', EntityType::class, array(
                'class' => 'MProdNexoEmpresaBundle\Entity\Empresa',
                'choice_label' => 'cuit',
                'placeholder' => 'Please choose',
                'empty_data' => null,
                'required' => false

            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MProdNexoEmpresaBundle\Entity\Publicacion'
        ));
    }
}
