<?php

namespace MProdNexoEmpresaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class UsuarioFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
            ->add('email', Filters\TextFilterType::class)
            ->add('estado', Filters\TextFilterType::class)
            ->add('password', Filters\TextFilterType::class)
            ->add('role', Filters\TextFilterType::class)
            ->add('createdAt', Filters\DateTimeFilterType::class)
            ->add('firstLogin', Filters\DateTimeFilterType::class)
            ->add('lastLogin', Filters\DateTimeFilterType::class)
        
        ;
        $builder->setMethod("GET");


    }

    public function getBlockPrefix()
    {
        return null;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
