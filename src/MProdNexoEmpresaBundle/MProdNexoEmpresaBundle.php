<?php

namespace MProdNexoEmpresaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MProdNexoEmpresaBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
